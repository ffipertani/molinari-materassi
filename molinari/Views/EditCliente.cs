﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using molinari.Models;

namespace molinari.Views
{
    public partial class EditCliente : Form
    {
        public Cliente Cliente { get; set; }
        private List<Vendita> lastSearch;

        public EditCliente(Cliente cliente)
        {
            InitializeComponent();
            Cliente = cliente;
            Init();
        }

        public void Init()
        {
            textBoxCitta.Text = Cliente.Citta;
            textBoxCognome.Text = Cliente.Cognome;
            textBoxIndirizzo.Text = Cliente.Indirizzo;
            textBoxNome.Text = Cliente.Nome;
            textBoxRecapito.Text = Cliente.Recapito;

          
            SearchVendite();

        }



        private void SearchVendite()
        {
            VenditaSearch vs = new VenditaSearch();
            vs.Clienti = new List<Cliente>();
            vs.Clienti.Add(Cliente);
            lastSearch = DatabaseManager.SearchVendite(vs);
            listView1.Items.Clear();
            foreach (Vendita vendita in lastSearch)
            {
                listView1.Items.Add(createItem(vendita));
            }

            setStatus();
        }

        private void setStatus()
        {
            float total = 0;
            foreach (Vendita v in lastSearch)
            {
                float val = 0;
                float.TryParse(v.Prezzo, out val);
                total += val;
            }
            labelTotali.Text = "Totale acquisti: " + lastSearch.Count + "   Totale prezzi: " + total;
        }

        private ListViewItem createItem(Vendita vendita)
        {
            ListViewItem item = new ListViewItem();
            ListViewItem.ListViewSubItemCollection items = item.SubItems;
            items.Clear();
            
            items.Add(createItem(item, vendita.Data));
            items.Add(createItem(item, vendita.Azienda.Descrizione));
            items.Add(createItem(item, vendita.Modello.Descrizione));
            items.Add(createItem(item, vendita.Tipo.Descrizione));
            items.Add(createItem(item, vendita.Misura.Descrizione));
            items.Add(createItem(item, vendita.Qta));
            items.Add(createItem(item, vendita.Prezzo));
            items.Add(createItem(item, vendita.IdMovim));
            return item;
        }



        private ListViewItem.ListViewSubItem createItem(ListViewItem item, String text)
        {
            return new ListViewItem.ListViewSubItem(item, text);
        }

        private void save()
        {
            Cliente cliente = new Cliente();
            cliente.Codice = Cliente.Codice;
            cliente.Citta = textBoxCitta.Text;
            cliente.Cognome = textBoxCognome.Text;
            cliente.Indirizzo = textBoxIndirizzo.Text;
            cliente.Nome = textBoxNome.Text;
            cliente.Recapito = textBoxRecapito.Text;
            Cliente = cliente;
           
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            save();

            DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void buttonApplica_Click(object sender, EventArgs e)
        {
            save();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            SearchVendite();
        }

        private void toolStripButtonNuovo_Click(object sender, EventArgs e)
        {
            Vendita vendita = new Vendita();
            vendita.Cliente = Cliente;
            EditVendita nc = new EditVendita(vendita);
            DialogResult res = nc.ShowDialog(this);
            if (res == DialogResult.OK)
            {
                DatabaseManager.InsertVendita(nc.Vendita);
                SearchVendite();
            }
        }

        private void toolStripButtonElimina_Click(object sender, EventArgs e)
        {
            int count = listView1.SelectedItems.Count;
            if (count > 0)
            {
                foreach (ListViewItem item in listView1.SelectedItems)
                {
                    Vendita vendita = new Vendita();
                    vendita.IdMovim = item.SubItems[7].Text;
                    DatabaseManager.DeleteVendita(vendita);
                    SearchVendite();
                }
            }
        }

        private void toolStripButtonModifica_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                String text = listView1.SelectedItems[0].SubItems[7].Text;
                Vendita vendita = DatabaseManager.LoadVendita(text);
                EditVendita ec = new EditVendita(vendita);
                DialogResult res = ec.ShowDialog(this);
                if (res == DialogResult.OK)
                {
                    DatabaseManager.UpdateVendita(ec.Vendita);
                    SearchVendite();
                }
            }
        }

        private void listView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                String text = listView1.SelectedItems[0].SubItems[7].Text;
                Vendita vendita = DatabaseManager.LoadVendita(text);
                EditVendita ec = new EditVendita(vendita);
                DialogResult res = ec.ShowDialog(this);
                if (res == DialogResult.OK)
                {
                    DatabaseManager.UpdateVendita(ec.Vendita);
                    SearchVendite();
                }
            }
        }
    }
}
