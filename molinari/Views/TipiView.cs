﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using molinari.Models;

namespace molinari.Views
{
    public class TipiView:GenericView
    {
        private System.Windows.Forms.ImageList imageList1;
        private System.ComponentModel.IContainer components;

        public TipiView(Form1 parent)
            : base(parent)
        {
            InitializeComponent();
            CreateImage = imageList1.Images[0];
            EditImage = imageList1.Images[1];
            DeleteImage = imageList1.Images[2];

        }
        protected override List<Generic> DoSearch()
        {
            return DatabaseManager.SearchTipi().ToList<Generic>();
        }
        protected override Generic DoCreate()
        {
            return new Tipo();
        }
        protected override void DoInsert(Generic generic)
        {
            DatabaseManager.InsertTipo((Tipo)generic);
        }
        protected override void DoDelete(Generic generic)
        {
            DatabaseManager.DeleteTipo((Tipo)generic);
        }
        protected override void DoUpdate(Generic generic)
        {
            DatabaseManager.UpdateTipo((Tipo)generic);
        }
        protected override Generic DoLoad(String code)
        {
            return DatabaseManager.LoadTipo(code);
        }
        protected override void DoChangeId(Generic generic)
        {
            DatabaseManager.ChangeIdTipo((Tipo)generic);
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TipiView));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "database-add-icon.png");
            this.imageList1.Images.SetKeyName(1, "database-edit-icon.png");
            this.imageList1.Images.SetKeyName(2, "database-delete-icon.png");
            // 
            // TipiView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Name = "TipiView";
            this.ResumeLayout(false);
            this.PerformLayout();

        }
    }
}
