﻿namespace molinari.Views
{
    partial class AdvancedSearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdvancedSearch));
            this.label1 = new System.Windows.Forms.Label();
            this.dateTimePickerDa = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerA = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.checkBoxAziende = new System.Windows.Forms.CheckBox();
            this.checkedListBoxAziende = new System.Windows.Forms.CheckedListBox();
            this.groupBoxAziende = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkedListBoxModelli = new System.Windows.Forms.CheckedListBox();
            this.checkBoxModelli = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkedListBoxMisure = new System.Windows.Forms.CheckedListBox();
            this.checkBoxMisure = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.checkedListBoxTipi = new System.Windows.Forms.CheckedListBox();
            this.checkBoxTipi = new System.Windows.Forms.CheckBox();
            this.buttonCerca = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonPulisci = new System.Windows.Forms.Button();
            this.groupBoxClienti = new System.Windows.Forms.GroupBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeaderCodice = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderCognome = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderNome = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderIndirizzo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderCitta = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderRecapito = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonAggiorna = new System.Windows.Forms.ToolStripButton();
            this.toolStripTextBoxSearch = new System.Windows.Forms.ToolStripTextBox();
            this.groupBoxAziende.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBoxClienti.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Data da";
            // 
            // dateTimePickerDa
            // 
            this.dateTimePickerDa.Location = new System.Drawing.Point(65, 12);
            this.dateTimePickerDa.Name = "dateTimePickerDa";
            this.dateTimePickerDa.Size = new System.Drawing.Size(202, 20);
            this.dateTimePickerDa.TabIndex = 1;
            this.dateTimePickerDa.Value = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            // 
            // dateTimePickerA
            // 
            this.dateTimePickerA.Location = new System.Drawing.Point(315, 13);
            this.dateTimePickerA.Name = "dateTimePickerA";
            this.dateTimePickerA.Size = new System.Drawing.Size(178, 20);
            this.dateTimePickerA.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(296, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "a";
            // 
            // checkBoxAziende
            // 
            this.checkBoxAziende.AutoSize = true;
            this.checkBoxAziende.Checked = true;
            this.checkBoxAziende.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxAziende.Location = new System.Drawing.Point(79, 19);
            this.checkBoxAziende.Name = "checkBoxAziende";
            this.checkBoxAziende.Size = new System.Drawing.Size(96, 17);
            this.checkBoxAziende.TabIndex = 5;
            this.checkBoxAziende.Text = "Seleziona tutto";
            this.checkBoxAziende.UseVisualStyleBackColor = true;
            this.checkBoxAziende.CheckedChanged += new System.EventHandler(this.checkBoxAziende_CheckedChanged);
            // 
            // checkedListBoxAziende
            // 
            this.checkedListBoxAziende.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkedListBoxAziende.Enabled = false;
            this.checkedListBoxAziende.FormattingEnabled = true;
            this.checkedListBoxAziende.Location = new System.Drawing.Point(6, 57);
            this.checkedListBoxAziende.Name = "checkedListBoxAziende";
            this.checkedListBoxAziende.Size = new System.Drawing.Size(238, 94);
            this.checkedListBoxAziende.TabIndex = 6;
            // 
            // groupBoxAziende
            // 
            this.groupBoxAziende.Controls.Add(this.checkedListBoxAziende);
            this.groupBoxAziende.Controls.Add(this.checkBoxAziende);
            this.groupBoxAziende.Location = new System.Drawing.Point(17, 65);
            this.groupBoxAziende.Name = "groupBoxAziende";
            this.groupBoxAziende.Size = new System.Drawing.Size(250, 173);
            this.groupBoxAziende.TabIndex = 7;
            this.groupBoxAziende.TabStop = false;
            this.groupBoxAziende.Text = "Aziende";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkedListBoxModelli);
            this.groupBox1.Controls.Add(this.checkBoxModelli);
            this.groupBox1.Location = new System.Drawing.Point(286, 65);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(250, 173);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Modelli";
            // 
            // checkedListBoxModelli
            // 
            this.checkedListBoxModelli.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkedListBoxModelli.Enabled = false;
            this.checkedListBoxModelli.FormattingEnabled = true;
            this.checkedListBoxModelli.Location = new System.Drawing.Point(6, 57);
            this.checkedListBoxModelli.Name = "checkedListBoxModelli";
            this.checkedListBoxModelli.Size = new System.Drawing.Size(238, 94);
            this.checkedListBoxModelli.TabIndex = 6;
            // 
            // checkBoxModelli
            // 
            this.checkBoxModelli.AutoSize = true;
            this.checkBoxModelli.Checked = true;
            this.checkBoxModelli.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxModelli.Location = new System.Drawing.Point(79, 19);
            this.checkBoxModelli.Name = "checkBoxModelli";
            this.checkBoxModelli.Size = new System.Drawing.Size(96, 17);
            this.checkBoxModelli.TabIndex = 5;
            this.checkBoxModelli.Text = "Seleziona tutto";
            this.checkBoxModelli.UseVisualStyleBackColor = true;
            this.checkBoxModelli.CheckedChanged += new System.EventHandler(this.checkBoxModelli_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkedListBoxMisure);
            this.groupBox2.Controls.Add(this.checkBoxMisure);
            this.groupBox2.Location = new System.Drawing.Point(17, 257);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(250, 173);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Misure";
            // 
            // checkedListBoxMisure
            // 
            this.checkedListBoxMisure.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkedListBoxMisure.Enabled = false;
            this.checkedListBoxMisure.FormattingEnabled = true;
            this.checkedListBoxMisure.Location = new System.Drawing.Point(6, 57);
            this.checkedListBoxMisure.Name = "checkedListBoxMisure";
            this.checkedListBoxMisure.Size = new System.Drawing.Size(238, 94);
            this.checkedListBoxMisure.TabIndex = 6;
            // 
            // checkBoxMisure
            // 
            this.checkBoxMisure.AutoSize = true;
            this.checkBoxMisure.Checked = true;
            this.checkBoxMisure.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxMisure.Location = new System.Drawing.Point(79, 19);
            this.checkBoxMisure.Name = "checkBoxMisure";
            this.checkBoxMisure.Size = new System.Drawing.Size(96, 17);
            this.checkBoxMisure.TabIndex = 5;
            this.checkBoxMisure.Text = "Seleziona tutto";
            this.checkBoxMisure.UseVisualStyleBackColor = true;
            this.checkBoxMisure.CheckedChanged += new System.EventHandler(this.checkBoxMisure_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.checkedListBoxTipi);
            this.groupBox3.Controls.Add(this.checkBoxTipi);
            this.groupBox3.Location = new System.Drawing.Point(286, 257);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(250, 173);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Tipi";
            // 
            // checkedListBoxTipi
            // 
            this.checkedListBoxTipi.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkedListBoxTipi.Enabled = false;
            this.checkedListBoxTipi.FormattingEnabled = true;
            this.checkedListBoxTipi.Location = new System.Drawing.Point(6, 57);
            this.checkedListBoxTipi.Name = "checkedListBoxTipi";
            this.checkedListBoxTipi.Size = new System.Drawing.Size(238, 94);
            this.checkedListBoxTipi.TabIndex = 6;
            // 
            // checkBoxTipi
            // 
            this.checkBoxTipi.AutoSize = true;
            this.checkBoxTipi.Checked = true;
            this.checkBoxTipi.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxTipi.Location = new System.Drawing.Point(79, 19);
            this.checkBoxTipi.Name = "checkBoxTipi";
            this.checkBoxTipi.Size = new System.Drawing.Size(96, 17);
            this.checkBoxTipi.TabIndex = 5;
            this.checkBoxTipi.Text = "Seleziona tutto";
            this.checkBoxTipi.UseVisualStyleBackColor = true;
            this.checkBoxTipi.CheckedChanged += new System.EventHandler(this.checkBoxTipi_CheckedChanged);
            // 
            // buttonCerca
            // 
            this.buttonCerca.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCerca.Location = new System.Drawing.Point(930, 486);
            this.buttonCerca.Name = "buttonCerca";
            this.buttonCerca.Size = new System.Drawing.Size(75, 23);
            this.buttonCerca.TabIndex = 14;
            this.buttonCerca.Text = "Cerca";
            this.buttonCerca.UseVisualStyleBackColor = true;
            this.buttonCerca.Click += new System.EventHandler(this.buttonCerca_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(849, 486);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 13;
            this.buttonCancel.Text = "Annulla";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonPulisci
            // 
            this.buttonPulisci.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonPulisci.Location = new System.Drawing.Point(12, 486);
            this.buttonPulisci.Name = "buttonPulisci";
            this.buttonPulisci.Size = new System.Drawing.Size(75, 23);
            this.buttonPulisci.TabIndex = 15;
            this.buttonPulisci.Text = "Pulisci";
            this.buttonPulisci.UseVisualStyleBackColor = true;
            this.buttonPulisci.Click += new System.EventHandler(this.buttonPulisci_Click);
            // 
            // groupBoxClienti
            // 
            this.groupBoxClienti.Controls.Add(this.listView1);
            this.groupBoxClienti.Controls.Add(this.toolStrip1);
            this.groupBoxClienti.Location = new System.Drawing.Point(560, 65);
            this.groupBoxClienti.Name = "groupBoxClienti";
            this.groupBoxClienti.Size = new System.Drawing.Size(454, 365);
            this.groupBoxClienti.TabIndex = 17;
            this.groupBoxClienti.TabStop = false;
            this.groupBoxClienti.Text = "Clienti";
            // 
            // listView1
            // 
            this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView1.CheckBoxes = true;
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderCodice,
            this.columnHeaderCognome,
            this.columnHeaderNome,
            this.columnHeaderIndirizzo,
            this.columnHeaderCitta,
            this.columnHeaderRecapito});
            this.listView1.FullRowSelect = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(6, 44);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(442, 315);
            this.listView1.TabIndex = 18;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.listView1_ItemCheck);
            // 
            // columnHeaderCodice
            // 
            this.columnHeaderCodice.Text = "Codice";
            this.columnHeaderCodice.Width = 48;
            // 
            // columnHeaderCognome
            // 
            this.columnHeaderCognome.Text = "Cognome";
            this.columnHeaderCognome.Width = 95;
            // 
            // columnHeaderNome
            // 
            this.columnHeaderNome.Text = "Nome";
            this.columnHeaderNome.Width = 75;
            // 
            // columnHeaderIndirizzo
            // 
            this.columnHeaderIndirizzo.Text = "Indirizzo";
            this.columnHeaderIndirizzo.Width = 124;
            // 
            // columnHeaderCitta
            // 
            this.columnHeaderCitta.Text = "Città";
            this.columnHeaderCitta.Width = 86;
            // 
            // columnHeaderRecapito
            // 
            this.columnHeaderRecapito.Text = "Recapito";
            this.columnHeaderRecapito.Width = 409;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonAggiorna,
            this.toolStripTextBoxSearch});
            this.toolStrip1.Location = new System.Drawing.Point(3, 16);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(448, 25);
            this.toolStrip1.TabIndex = 17;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButtonAggiorna
            // 
            this.toolStripButtonAggiorna.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonAggiorna.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAggiorna.Image")));
            this.toolStripButtonAggiorna.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAggiorna.Name = "toolStripButtonAggiorna";
            this.toolStripButtonAggiorna.Size = new System.Drawing.Size(57, 22);
            this.toolStripButtonAggiorna.Text = "Cerca";
            this.toolStripButtonAggiorna.Click += new System.EventHandler(this.toolStripButtonAggiorna_Click);
            // 
            // toolStripTextBoxSearch
            // 
            this.toolStripTextBoxSearch.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripTextBoxSearch.Name = "toolStripTextBoxSearch";
            this.toolStripTextBoxSearch.Size = new System.Drawing.Size(200, 25);
            this.toolStripTextBoxSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.toolStripTextBoxSearch_KeyDown);
            // 
            // AdvancedSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(1026, 521);
            this.Controls.Add(this.groupBoxClienti);
            this.Controls.Add(this.buttonPulisci);
            this.Controls.Add(this.buttonCerca);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBoxAziende);
            this.Controls.Add(this.dateTimePickerA);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dateTimePickerDa);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "AdvancedSearch";
            this.Text = "Ricerca avanzata";
            this.Load += new System.EventHandler(this.AdvancedSearch_Load);
            this.groupBoxAziende.ResumeLayout(false);
            this.groupBoxAziende.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBoxClienti.ResumeLayout(false);
            this.groupBoxClienti.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateTimePickerDa;
        private System.Windows.Forms.DateTimePicker dateTimePickerA;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox checkBoxAziende;
        private System.Windows.Forms.CheckedListBox checkedListBoxAziende;
        private System.Windows.Forms.GroupBox groupBoxAziende;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckedListBox checkedListBoxModelli;
        private System.Windows.Forms.CheckBox checkBoxModelli;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckedListBox checkedListBoxMisure;
        private System.Windows.Forms.CheckBox checkBoxMisure;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckedListBox checkedListBoxTipi;
        private System.Windows.Forms.CheckBox checkBoxTipi;
        private System.Windows.Forms.Button buttonCerca;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonPulisci;
        private System.Windows.Forms.GroupBox groupBoxClienti;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButtonAggiorna;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxSearch;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeaderCodice;
        private System.Windows.Forms.ColumnHeader columnHeaderCognome;
        private System.Windows.Forms.ColumnHeader columnHeaderNome;
        private System.Windows.Forms.ColumnHeader columnHeaderIndirizzo;
        private System.Windows.Forms.ColumnHeader columnHeaderCitta;
        private System.Windows.Forms.ColumnHeader columnHeaderRecapito;
    }
}