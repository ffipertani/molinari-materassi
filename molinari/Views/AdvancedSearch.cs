﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using molinari.Models;

namespace molinari.Views
{
    public partial class AdvancedSearch : Form
    {
        public VenditaSearch VenditaSearch { get; private set; }
        private List<Cliente> lastSearch;
        private List<Cliente> clientiSelezionati = new List<Cliente>();
        private CheckBox cb = new CheckBox();

        public AdvancedSearch()
        {
            InitializeComponent();
            init();
        }

        private void init()
        {
            List<Azienda> aziende = DatabaseManager.SearchAziende();
            List<Modello> modelli = DatabaseManager.SearchModelli();
            List<Misura> misure = DatabaseManager.SearchMisure();
            List<Tipo> tipi = DatabaseManager.SearchTipi();
            lastSearch = DatabaseManager.SearchClienti();
            toolStripTextBoxSearch.Text = "";
            listView1.Items.Clear();
            foreach (Cliente cliente in lastSearch)
            {
                listView1.Items.Add(createItem(cliente));
            }


            clientiSelezionati.Clear();

            checkBoxAziende.Checked = true;
            checkedListBoxAziende.Items.Clear();
            checkedListBoxAziende.Items.AddRange(aziende.ToArray());
            checkBoxModelli.Checked = true;
            checkedListBoxModelli.Items.Clear();
            checkedListBoxModelli.Items.AddRange(modelli.ToArray());
            checkBoxMisure.Checked = true;
            checkedListBoxMisure.Items.Clear();
            checkedListBoxMisure.Items.AddRange(misure.ToArray());
            checkBoxTipi.Checked = true;
            checkedListBoxTipi.Items.Clear();
            checkedListBoxTipi.Items.AddRange(tipi.ToArray());

            dateTimePickerDa.Value = new DateTime(2000, 1, 1);
            dateTimePickerA.Value = DateTime.Today;
        }

        private ListViewItem createItem(Cliente cliente)
        {
            ListViewItem item = new ListViewItem();
            ListViewItem.ListViewSubItemCollection items = item.SubItems;
            items.Clear();
            items.Add(createItem(item, cliente.Codice));
            items.Add(createItem(item, cliente.Cognome));
            items.Add(createItem(item, cliente.Nome));
            items.Add(createItem(item, cliente.Indirizzo));
            items.Add(createItem(item, cliente.Citta));
            items.Add(createItem(item, cliente.Recapito));
            return item;
        }

        private ListViewItem.ListViewSubItem createItem(ListViewItem item, String text)
        {
            return new ListViewItem.ListViewSubItem(item, text);
        }

        private void buttonCerca_Click(object sender, EventArgs e)
        {
            VenditaSearch = new VenditaSearch();
            VenditaSearch.Da = dateTimePickerDa.Value;
            VenditaSearch.A = dateTimePickerA.Value;
            if (!checkBoxAziende.Checked)
            {
                VenditaSearch.Aziende = new List<Azienda>();
                foreach (Azienda az in checkedListBoxAziende.CheckedItems)
                {
                    VenditaSearch.Aziende.Add(az);
                }
                
            }
            if (!checkBoxModelli.Checked)
            {
                VenditaSearch.Modelli = new List<Modello>();
                foreach (Modello az in checkedListBoxModelli.CheckedItems)
                {
                    VenditaSearch.Modelli.Add(az);
                }

            }
            if (!checkBoxMisure.Checked)
            {
                VenditaSearch.Misure = new List<Misura>();
                foreach (Misura az in checkedListBoxMisure.CheckedItems)
                {
                    VenditaSearch.Misure.Add(az);
                }

            }
            if (!checkBoxTipi.Checked)
            {
                VenditaSearch.Tipi = new List<Tipo>();
                foreach (Tipo az in checkedListBoxTipi.CheckedItems)
                {
                    VenditaSearch.Tipi.Add(az);
                }

            }
            if (!cb.Checked)
            {
                VenditaSearch.Clienti = new List<Cliente>();
                foreach (Cliente c in clientiSelezionati)
                {
                    VenditaSearch.Clienti.Add(c);
                }
            }
            DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void checkBoxModelli_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxModelli.Checked)
            {
                for (int i = 0; i < checkedListBoxModelli.Items.Count; i++)
                {
                    checkedListBoxModelli.SetItemChecked(i, false);
                }
                checkedListBoxModelli.Enabled = false;
            }
            else
            {
                checkedListBoxModelli.Enabled = true;
            }
        }

        private void checkBoxAziende_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxAziende.Checked)
            {
                for (int i = 0; i < checkedListBoxAziende.Items.Count; i++)
                {
                    checkedListBoxAziende.SetItemChecked(i, false);
                }
                checkedListBoxAziende.Enabled = false;
            }
            else
            {
                checkedListBoxAziende.Enabled = true;
            }
        }

        private void checkBoxMisure_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxMisure.Checked)
            {
                for (int i = 0; i < checkedListBoxMisure.Items.Count; i++)
                {
                    checkedListBoxMisure.SetItemChecked(i, false);
                }
                checkedListBoxMisure.Enabled = false;
            }
            else
            {
                checkedListBoxMisure.Enabled = true;
            }
        }

        private void checkBoxTipi_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxTipi.Checked)
            {
                for (int i = 0; i < checkedListBoxTipi.Items.Count; i++)
                {
                    checkedListBoxTipi.SetItemChecked(i, false);
                }
                checkedListBoxTipi.Enabled = false;
            }
            else
            {
                checkedListBoxTipi.Enabled = true;
            }
        }

        private void buttonPulisci_Click(object sender, EventArgs e)
        {
            init();
        }

        private void toolStripButtonAggiorna_Click(object sender, EventArgs e)
        {
            String text = toolStripTextBoxSearch.Text;
            SearchClienti(text);
        }

        public void SearchClienti(String text)
        {
            lastSearch = DatabaseManager.SearchClienti(text);
            listView1.Items.Clear();
            foreach (Cliente cliente in lastSearch)
            {
                listView1.Items.Add(createItem(cliente));
            }
        }

        private void toolStripTextBoxSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                String text = toolStripTextBoxSearch.Text;
                SearchClienti(text);
                e.Handled = true;
                e.SuppressKeyPress = true;
            }

        }

        private void listView1_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            Cliente c = lastSearch[e.Index];
            if (e.NewValue == CheckState.Checked)
            {
                clientiSelezionati.Add(c);
            }
            else
            {
                foreach (Cliente uncliente in clientiSelezionati)
                {
                    if (c.Codice.Equals(uncliente.Codice))
                    {
                        clientiSelezionati.Remove(uncliente);
                        break;
                    }
                }
            }
        }

        private void AdvancedSearch_Load(object sender, EventArgs e)
        {
            cb.Text = "Seleziona tutto";
            cb.BackColor = Color.Transparent;
            cb.Checked = true;
           
            cb.CheckStateChanged += cb_CheckStateChanged;
            ToolStripControlHost host = new ToolStripControlHost(cb);
            toolStrip1.Items.Insert(0,host);
            cb_CheckStateChanged(cb, null);
        }

        void cb_CheckStateChanged(object sender, EventArgs e)
        {
            if (cb.Checked)
            {
                clientiSelezionati.Clear();
                foreach (ListViewItem item in listView1.Items)
                {
                    item.Checked = false;
                }
                listView1.Enabled = false;
                toolStripButtonAggiorna.Enabled = false;
                toolStripTextBoxSearch.Enabled = false;
            }
            else
            {
                listView1.Enabled = true;
                toolStripButtonAggiorna.Enabled = true;
                toolStripTextBoxSearch.Enabled = true;
            }
        }


    }
}
