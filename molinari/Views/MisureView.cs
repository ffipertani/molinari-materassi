﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using molinari.Models;

namespace molinari.Views
{
    public class MisureView:GenericView
    {
        private ImageList imageList1;
        private System.ComponentModel.IContainer components;

        public MisureView(Form1 parent):base(parent)
        {
            InitializeComponent();
            CreateImage = imageList1.Images[0];
            EditImage = imageList1.Images[1];
            DeleteImage = imageList1.Images[2];

        }
        protected override List<Generic> DoSearch()
        {
            return DatabaseManager.SearchMisure().ToList<Generic>();
        }
        protected override Generic DoCreate()
        {
            return new Misura();
        }
        protected override void DoInsert(Generic generic)
        {
            DatabaseManager.InsertMisura((Misura)generic);
        }
        protected override void DoDelete(Generic generic)
        {
            DatabaseManager.DeleteMisura((Misura)generic);
        }
        protected override void DoUpdate(Generic generic)
        {
            DatabaseManager.UpdateMisura((Misura)generic);
        }
        protected override Generic DoLoad(String code)
        {
            return DatabaseManager.LoadMisura(code);
        }
        protected override void DoChangeId(Generic generic)
        {
            DatabaseManager.ChangeIdMisura((Misura)generic);
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MisureView));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "calculator-add-icon.png");
            this.imageList1.Images.SetKeyName(1, "calculator-edit-icon.png");
            this.imageList1.Images.SetKeyName(2, "calculator-delete-icon.png");
            // 
            // MisureView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Name = "MisureView";
            this.ResumeLayout(false);
            this.PerformLayout();

        }
       
    }
}
