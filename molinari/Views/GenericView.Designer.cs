﻿namespace molinari.Views
{
    partial class GenericView
    {
        /// <summary> 
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Liberare le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary> 
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GenericView));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonNuovo = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonElimina = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonModifica = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonAggiorna = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonCorreggi = new System.Windows.Forms.ToolStripButton();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeaderCodice = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderDescrizione = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonNuovo,
            this.toolStripButtonElimina,
            this.toolStripButtonModifica,
            this.toolStripButtonAggiorna,
            this.toolStripButtonCorreggi});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(480, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButtonNuovo
            // 
            this.toolStripButtonNuovo.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonNuovo.Image")));
            this.toolStripButtonNuovo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonNuovo.Name = "toolStripButtonNuovo";
            this.toolStripButtonNuovo.Size = new System.Drawing.Size(63, 22);
            this.toolStripButtonNuovo.Text = "Nuovo";
            this.toolStripButtonNuovo.Click += new System.EventHandler(this.toolStripButtonNuovo_Click);
            // 
            // toolStripButtonElimina
            // 
            this.toolStripButtonElimina.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonElimina.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonElimina.Image")));
            this.toolStripButtonElimina.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonElimina.Name = "toolStripButtonElimina";
            this.toolStripButtonElimina.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonElimina.Text = "Elimina";
            this.toolStripButtonElimina.Click += new System.EventHandler(this.toolStripButtonElimina_Click);
            // 
            // toolStripButtonModifica
            // 
            this.toolStripButtonModifica.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonModifica.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonModifica.Image")));
            this.toolStripButtonModifica.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonModifica.Name = "toolStripButtonModifica";
            this.toolStripButtonModifica.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonModifica.Text = "Modifica";
            this.toolStripButtonModifica.Click += new System.EventHandler(this.toolStripButtonModifica_Click);
            // 
            // toolStripButtonAggiorna
            // 
            this.toolStripButtonAggiorna.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonAggiorna.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAggiorna.Image")));
            this.toolStripButtonAggiorna.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAggiorna.Name = "toolStripButtonAggiorna";
            this.toolStripButtonAggiorna.Size = new System.Drawing.Size(76, 22);
            this.toolStripButtonAggiorna.Text = "Aggiorna";
            this.toolStripButtonAggiorna.Click += new System.EventHandler(this.toolStripButtonAggiorna_Click);
            // 
            // toolStripButtonCorreggi
            // 
            this.toolStripButtonCorreggi.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonCorreggi.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonCorreggi.Image")));
            this.toolStripButtonCorreggi.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCorreggi.Name = "toolStripButtonCorreggi";
            this.toolStripButtonCorreggi.Size = new System.Drawing.Size(94, 22);
            this.toolStripButtonCorreggi.Text = "Correggi Codici";
            this.toolStripButtonCorreggi.Visible = false;
            this.toolStripButtonCorreggi.Click += new System.EventHandler(this.toolStripButtonCorreggi_Click);
            // 
            // listView1
            // 
            this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderCodice,
            this.columnHeaderDescrizione});
            this.listView1.FullRowSelect = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(0, 28);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(477, 297);
            this.listView1.TabIndex = 1;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listView1_MouseDoubleClick);
            // 
            // columnHeaderCodice
            // 
            this.columnHeaderCodice.Text = "Codice";
            // 
            // columnHeaderDescrizione
            // 
            this.columnHeaderDescrizione.Text = "Descrizione";
            this.columnHeaderDescrizione.Width = 388;
            // 
            // GenericView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "GenericView";
            this.Size = new System.Drawing.Size(480, 328);
            this.VisibleChanged += new System.EventHandler(this.GenericView_VisibleChanged);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeaderCodice;
        private System.Windows.Forms.ColumnHeader columnHeaderDescrizione;
        private System.Windows.Forms.ToolStripButton toolStripButtonNuovo;
        private System.Windows.Forms.ToolStripButton toolStripButtonElimina;
        private System.Windows.Forms.ToolStripButton toolStripButtonModifica;
        private System.Windows.Forms.ToolStripButton toolStripButtonAggiorna;
        private System.Windows.Forms.ToolStripButton toolStripButtonCorreggi;
    }
}
