﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using molinari.Models;

namespace molinari.Views
{
    public partial class NuovoCliente : Form
    {
        public Cliente Cliente { get; set; }
        public NuovoCliente()
        {
            InitializeComponent();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            Cliente cliente = new Cliente();
            cliente.Citta = textBoxCitta.Text;
            cliente.Cognome = textBoxCognome.Text;
            cliente.Indirizzo = textBoxIndirizzo.Text;
            cliente.Nome = textBoxNome.Text;
            cliente.Recapito = textBoxRecapito.Text;

            Cliente = cliente;
            DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }
    }
}
