﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using molinari.Models;

namespace molinari.Views
{
    public partial class EditVendita : Form
    {
        public Vendita Vendita { get; set; }

        public EditVendita()
        {
            InitializeComponent();
            Vendita = new Vendita();
            init();
        }

        public EditVendita(Vendita vendita)
        {
            InitializeComponent();
            Vendita = vendita;
            init();
        }

        private void init()
        {
            List<Azienda> aziende = DatabaseManager.SearchAziende();
            List<Modello> modelli = DatabaseManager.SearchModelli();
            List<Misura> misure = DatabaseManager.SearchMisure();
            List<Tipo> tipi = DatabaseManager.SearchTipi();
            List<Cliente> clienti = DatabaseManager.SearchClienti();

            comboBoxAzienda.Items.AddRange(toArray(aziende));
            comboBoxModello.Items.AddRange(toArray(modelli));
            comboBoxMisura.Items.AddRange(toArray(misure));
            comboBoxTipo.Items.AddRange(toArray(tipi));
            comboBoxClienti.Items.AddRange(toArray(clienti));


            foreach (ComboBoxItem item in comboBoxAzienda.Items)
            {
                String codice = Vendita.Azienda.Codice;
                if (codice != null && codice.Equals(((Azienda)item.Value).Codice))
                {
                    comboBoxAzienda.SelectedItem = item;
                    break;
                }

            }
            foreach (ComboBoxItem item in comboBoxModello.Items)
            {
                String codice = Vendita.Modello.Codice;
                if (codice != null && codice.Equals(((Modello)item.Value).Codice))
                {
                    comboBoxModello.SelectedItem = item;
                    break;
                }
               
            }
            foreach (ComboBoxItem item in comboBoxMisura.Items)
            {
                String codice = Vendita.Misura.Codice;
                if (codice != null && codice.Equals(((Misura)item.Value).Codice))
                {
                    comboBoxMisura.SelectedItem = item;
                    break;
                }

            }
            foreach (ComboBoxItem item in comboBoxTipo.Items)
            {
                String codice = Vendita.Tipo.Codice;
                if (codice != null && codice.Equals(((Tipo)item.Value).Codice))
                {
                    comboBoxTipo.SelectedItem = item;
                    break;
                }

            }
            foreach (ComboBoxItem item in comboBoxClienti.Items)
            {
                String codice = Vendita.Cliente.Codice;
                if (codice != null && codice.Equals(((Cliente)item.Value).Codice))
                {
                    comboBoxClienti.SelectedItem = item;
                    break;
                }

            }
            comboBoxModello.SelectedText =Vendita.Modello.Descrizione;
            comboBoxMisura.SelectedText = Vendita.Misura.Descrizione;
            comboBoxTipo.SelectedText =Vendita.Tipo.Descrizione;  

            textBoxQta.Text = Vendita.Qta;
            textBoxPrezzo.Text = Vendita.Prezzo;
            if (Vendita.Data != null) { 
                dateTimePicker.Value = DateTime.Parse(Vendita.Data);
            }
        }

        private ComboBoxItem[] toArray(List<Azienda> list)
        {
            List<ComboBoxItem> newlist = new List<ComboBoxItem>();
            foreach (Azienda gen in list)
            {
                newlist.Add(createItem(gen));
            }
            return newlist.ToArray<ComboBoxItem>();
        }

        private ComboBoxItem[] toArray(List<Cliente> list)
        {
            List<ComboBoxItem> newlist = new List<ComboBoxItem>();
            foreach (Cliente gen in list)
            {
                newlist.Add(createItem(gen));
            }
            return newlist.ToArray<ComboBoxItem>();
        }

        private ComboBoxItem[] toArray(List<Modello> list)
        {
            List<ComboBoxItem> newlist = new List<ComboBoxItem>();
            foreach (Modello gen in list)
            {
                newlist.Add(createItem(gen));
            }
            return newlist.ToArray<ComboBoxItem>();
        }

        private ComboBoxItem[] toArray(List<Misura> list)
        {
            List<ComboBoxItem> newlist = new List<ComboBoxItem>();
            foreach (Misura gen in list)
            {
                newlist.Add(createItem(gen));
            }
            return newlist.ToArray<ComboBoxItem>();
        }

        private ComboBoxItem[] toArray(List<Tipo> list)
        {
            List<ComboBoxItem> newlist = new List<ComboBoxItem>();
            foreach (Tipo gen in list)
            {
                newlist.Add(createItem(gen));
            }
            return newlist.ToArray<ComboBoxItem>();
        }

        private ComboBoxItem createItem(Generic gen)
        {
            ComboBoxItem c = new ComboBoxItem();
            c.Text = gen.Descrizione;
            c.Value = gen;
            return c;
        }

        private ComboBoxItem createItem(Cliente gen)
        {
            ComboBoxItem c = new ComboBoxItem();
            c.Text = gen.Cognome + " " + gen.Nome;
            c.Value = gen;
            return c;
        }

        private void save()
        {
            Vendita vendita = new Vendita();
            vendita.Cliente = (Cliente)((ComboBoxItem)comboBoxClienti.SelectedItem).Value;
            vendita.Azienda = (Azienda) ((ComboBoxItem) comboBoxAzienda.SelectedItem).Value;
            vendita.Modello = (Modello)((ComboBoxItem)comboBoxModello.SelectedItem).Value;
            vendita.Misura = (Misura)((ComboBoxItem)comboBoxMisura.SelectedItem).Value;
            vendita.Tipo = (Tipo)((ComboBoxItem)comboBoxTipo.SelectedItem).Value;
            vendita.Qta = textBoxQta.Text;
            vendita.Prezzo = textBoxPrezzo.Text;
            vendita.Data = dateTimePicker.Value.ToString("dd/MM/yyyy");
            vendita.IdMovim = Vendita.IdMovim;
            Vendita = vendita;

        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            save();

            DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void buttonApplica_Click(object sender, EventArgs e)
        {
            save();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

       
    }

    public class ComboBoxItem
    {
        public string Text { get; set; }
        public object Value { get; set; }
        public ComboBoxItem() { }
        public ComboBoxItem(String text, object value)
        {
            Text = text;
            Value = value;
        }
        
        public override string ToString()
        {
            return Text;
        }
    }
}
