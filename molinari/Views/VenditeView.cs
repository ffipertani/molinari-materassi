﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using molinari.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using iTextSharp.text.html.simpleparser;
using molinari.Dao;

namespace molinari.Views
{
    public partial class VenditeView : UserControl
    {
        private List<Vendita> lastSearch;
        private Form1 parent;

        public VenditeView(Form1 parent)
        {
            InitializeComponent();
            this.parent = parent;
            SearchVendite();

        }

        public VenditeView()
        {
            InitializeComponent();
            SearchVendite();
            
        }

        private void setStatus()
        {
            float total = 0;
            foreach (Vendita v in lastSearch)
            {
                float val = 0;
                float.TryParse(v.Prezzo, out val);
                total += val;
            }
            if (this.Visible) { 
                parent.SetStatus("Record totali: " + lastSearch.Count+"     Totale prezzi: " +total );
            }
        }

        public void SearchVendite()
        {
            lastSearch = DatabaseManager.SearchVendite();
            List<Cliente> clienti = DatabaseManager.SearchClienti();
           listView1.Items.Clear();
           foreach (Vendita vendita in lastSearch)
            {

                listView1.Items.Add(createItem(vendita));
            }
          setStatus();
        }


        private ListViewItem createItem(Vendita vendita)
        {
            ListViewItem item = new ListViewItem();
            ListViewItem.ListViewSubItemCollection items = item.SubItems;
            items.Clear();
            if (vendita.Cliente != null) { 
                items.Add(createItem(item, vendita.Cliente.Cognome + " " + vendita.Cliente.Nome));
            }
            else
            {
                items.Add(createItem(item, ""));
            }
            items.Add(createItem(item, vendita.Data));
            items.Add(createItem(item, vendita.Azienda.Descrizione));
            items.Add(createItem(item, vendita.Modello.Descrizione));
            items.Add(createItem(item, vendita.Tipo.Descrizione));
            items.Add(createItem(item, vendita.Misura.Descrizione));
            items.Add(createItem(item, vendita.Qta));
            items.Add(createItem(item, vendita.Prezzo));
            items.Add(createItem(item, vendita.IdMovim));
            return item;
        }

       

        private ListViewItem.ListViewSubItem createItem(ListViewItem item, String text)
        {
            return new ListViewItem.ListViewSubItem(item, text);
        }

        private void toolStripButtonNuovo_Click(object sender, EventArgs e)
        {
            EditVendita nc = new EditVendita();
            DialogResult res = nc.ShowDialog(this);
            if (res == DialogResult.OK)
            {
                DatabaseManager.InsertVendita(nc.Vendita);
                listView1.Items.Insert(0, createItem(nc.Vendita));
            }
        }

        private void toolStripButtonAggiorna_Click(object sender, EventArgs e)
        {
            SearchVendite();
        }

        private void toolStripButtonModifica_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                String text = listView1.SelectedItems[0].SubItems[8].Text;
                Vendita vendita = DatabaseManager.LoadVendita(text);
                EditVendita ec = new EditVendita(vendita);
                 DialogResult res = ec.ShowDialog(this);
                if (res == DialogResult.OK)
                {
                    DatabaseManager.UpdateVendita(ec.Vendita);
                    SearchVendite();
                }
            }
           

        }

        private void toolStripButtonElimina_Click(object sender, EventArgs e)
        {
            int count =  listView1.SelectedItems.Count;
            if(count>0){
                foreach (ListViewItem item in listView1.SelectedItems)
                {
                    Vendita vendita = new Vendita();
                    vendita.IdMovim = item.SubItems[8].Text;
                    DatabaseManager.DeleteVendita(vendita);
                    listView1.Items.Remove(item);
                }
            }

           
        }

        private void toolStripButtonAdvanced_Click(object sender, EventArgs e)
        {
            AdvancedSearch ads = new AdvancedSearch();
            DialogResult res = ads.ShowDialog(this);
            if (res == DialogResult.OK)
            {
                VenditaSearch vs = ads.VenditaSearch;
                 lastSearch = DatabaseManager.SearchVendite(vs);
                 List<Cliente> clienti = DatabaseManager.SearchClienti();
                listView1.Items.Clear();
                foreach (Vendita vendita in lastSearch)
                {
                    listView1.Items.Add(createItem(vendita));
                }
                setStatus();
            }
        }

        private void toolStripButtonConverti_Click(object sender, EventArgs e)
        {
            DatabaseManager.ConvertDataVendite();
            SearchVendite();
        }

        private void toolStripButtonStatistiche_Click(object sender, EventArgs e)
        {
            MemoryStream ms = new MemoryStream();
            Document doc = new Document();
            PdfWriter writer = PdfWriter.GetInstance(doc, ms);
            doc.Open();

            //Our sample HTML and CSS
            //var example_html = @"<p>This <em>is </em><span class=""headline"" style=""text-decoration: underline;"">some</span> <strong>sample <em> text</em></strong><span style=""color: red;"">!!!</span></p>";
            var example_html = @"<p style=""margin-bottom:20px""><strong>Statistiche ricerca del "+ DateTime.Today.ToString("dd/MM/yyyy")+"</strong></p>";
            var example_css = @".headline{font-size:200%}";

            int total = lastSearch.Count;
            int totalQta = 0;
            int totalPrezzo = 0;
            Dictionary<String, Azienda> aziende = new Dictionary<string, Azienda>();
            foreach (Vendita v in lastSearch)
            {
                try
                {
                    int qta = Int32.Parse(v.Qta);
                    totalQta += qta;
                }
                catch (Exception ex){}
                try
                {
                    int prezzo = Int32.Parse(v.Prezzo);
                    totalPrezzo += prezzo;
                }
                catch (Exception ex) { }
               Azienda a;
               if (!aziende.TryGetValue(v.Azienda.Codice, out a))
               {

               }
            }

            example_html += "<p>Totale risultati: <b>" + total + "</b></p>";
            example_html += "<p>Totale quantità: <b>" + totalQta + "</b></p>";
            example_html += "<p>Totale prezzi: <b>" + totalPrezzo + "</b></p>";

          
           

            var msCss = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(example_css));
            var msHtml = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(example_html));
            iTextSharp.tool.xml.XMLWorkerHelper.GetInstance().ParseXHtml(writer, doc, msHtml, msCss);

            doc.Close();

            byte[] bytes = ms.ToArray();

            var testFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "test.pdf");
            System.IO.File.WriteAllBytes(testFile, bytes);
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            //Crea id delle vendite
            VenditaDao dao = new VenditaDao();
            dao.CreateId();
        }

        private void listView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                String text = listView1.SelectedItems[0].SubItems[8].Text;
                Vendita vendita = DatabaseManager.LoadVendita(text);
                EditVendita ec = new EditVendita(vendita);
                DialogResult res = ec.ShowDialog(this);
                if (res == DialogResult.OK)
                {
                    DatabaseManager.UpdateVendita(ec.Vendita);
                    SearchVendite();
                }
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            SearchVendite();
        }

        private void listView1_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                if (lastSearch == null)
                {
                    SearchVendite();
                }
                setStatus();
            }
        }
    }
}
