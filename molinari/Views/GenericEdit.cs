﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using molinari.Models;

namespace molinari.Views
{
    public partial class GenericEdit : Form
    {
        public Generic Object { get; set; }
        public GenericEdit(Generic generic)
        {
            InitializeComponent();
            Object = generic;
            Init();

          
        }

        public void Init()
        {
            textBoxNome.Text = Object.Descrizione;
           
        }

        private void save()
        {
            Generic cliente = new Generic();
            cliente.Codice = Object.Codice;
            cliente.Descrizione = textBoxNome.Text;

            Object = cliente;

        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            save();

            DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void buttonApplica_Click(object sender, EventArgs e)
        {
            save();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
    }
}
