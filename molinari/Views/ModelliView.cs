﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using molinari.Models;

namespace molinari.Views
{
    public class ModelliView:GenericView
    {
        private System.Windows.Forms.ImageList imageList1;
        private System.ComponentModel.IContainer components;

        public ModelliView(Form1 parent)
            : base(parent)
        {
            InitializeComponent();
            CreateImage = imageList1.Images[0];
            EditImage = imageList1.Images[1];
            DeleteImage = imageList1.Images[2];
        }

        protected override List<Generic> DoSearch()
        {
            return DatabaseManager.SearchModelli().ToList<Generic>();
        }
        protected override Generic DoCreate()
        {
            return new Modello();
        }
        protected override void DoInsert(Generic generic)
        {
            DatabaseManager.InsertModello((Modello)generic);
        }
        protected override void DoDelete(Generic generic)
        {
            DatabaseManager.DeleteModello((Modello)generic);
        }
        protected override void DoUpdate(Generic generic)
        {
            DatabaseManager.UpdateModello((Modello)generic);
        }
        protected override Generic DoLoad(String code)
        {
            return DatabaseManager.LoadModello(code);
        }
        protected override void DoChangeId(Generic generic)
        {
            DatabaseManager.ChangeIdModello((Modello)generic);
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModelliView));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "layout-add-icon.png");
            this.imageList1.Images.SetKeyName(1, "layout-edit-icon.png");
            this.imageList1.Images.SetKeyName(2, "layout-delete-icon.png");
            // 
            // ModelliView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Name = "ModelliView";
            this.ResumeLayout(false);
            this.PerformLayout();

        }
    }
}
