﻿namespace molinari.Views
{
    partial class VenditeView
    {
        /// <summary> 
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Liberare le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary> 
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VenditeView));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonNuovo = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonElimina = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonModifica = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonAdvanced = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonConverti = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonStatistiche = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeaderCodice = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderData = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderAzienda = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderModello = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderTipo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderMisura = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderQta = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderPrezzo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonNuovo,
            this.toolStripButtonElimina,
            this.toolStripButtonModifica,
            this.toolStripButtonAdvanced,
            this.toolStripButton1,
            this.toolStripButtonConverti,
            this.toolStripButtonStatistiche,
            this.toolStripButton2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(618, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButtonNuovo
            // 
            this.toolStripButtonNuovo.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonNuovo.Image")));
            this.toolStripButtonNuovo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonNuovo.Name = "toolStripButtonNuovo";
            this.toolStripButtonNuovo.Size = new System.Drawing.Size(63, 22);
            this.toolStripButtonNuovo.Text = "Nuovo";
            this.toolStripButtonNuovo.Click += new System.EventHandler(this.toolStripButtonNuovo_Click);
            // 
            // toolStripButtonElimina
            // 
            this.toolStripButtonElimina.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonElimina.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonElimina.Image")));
            this.toolStripButtonElimina.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonElimina.Name = "toolStripButtonElimina";
            this.toolStripButtonElimina.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonElimina.Text = "Elimina";
            this.toolStripButtonElimina.Click += new System.EventHandler(this.toolStripButtonElimina_Click);
            // 
            // toolStripButtonModifica
            // 
            this.toolStripButtonModifica.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonModifica.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonModifica.Image")));
            this.toolStripButtonModifica.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonModifica.Name = "toolStripButtonModifica";
            this.toolStripButtonModifica.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonModifica.Text = "Modifica";
            this.toolStripButtonModifica.Click += new System.EventHandler(this.toolStripButtonModifica_Click);
            // 
            // toolStripButtonAdvanced
            // 
            this.toolStripButtonAdvanced.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonAdvanced.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAdvanced.Image")));
            this.toolStripButtonAdvanced.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAdvanced.Name = "toolStripButtonAdvanced";
            this.toolStripButtonAdvanced.Size = new System.Drawing.Size(114, 22);
            this.toolStripButtonAdvanced.Text = "Ricerca avanzata";
            this.toolStripButtonAdvanced.Click += new System.EventHandler(this.toolStripButtonAdvanced_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(85, 22);
            this.toolStripButton1.Text = "Carica tutti";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButtonConverti
            // 
            this.toolStripButtonConverti.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonConverti.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonConverti.Name = "toolStripButtonConverti";
            this.toolStripButtonConverti.Size = new System.Drawing.Size(82, 22);
            this.toolStripButtonConverti.Text = "Converti date";
            this.toolStripButtonConverti.Visible = false;
            this.toolStripButtonConverti.Click += new System.EventHandler(this.toolStripButtonConverti_Click);
            // 
            // toolStripButtonStatistiche
            // 
            this.toolStripButtonStatistiche.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonStatistiche.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonStatistiche.Image")));
            this.toolStripButtonStatistiche.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonStatistiche.Name = "toolStripButtonStatistiche";
            this.toolStripButtonStatistiche.Size = new System.Drawing.Size(65, 22);
            this.toolStripButtonStatistiche.Text = "Statistiche";
            this.toolStripButtonStatistiche.Visible = false;
            this.toolStripButtonStatistiche.Click += new System.EventHandler(this.toolStripButtonStatistiche_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(58, 22);
            this.toolStripButton2.Text = "Create id";
            this.toolStripButton2.Visible = false;
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // listView1
            // 
            this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderCodice,
            this.columnHeaderData,
            this.columnHeaderAzienda,
            this.columnHeaderModello,
            this.columnHeaderTipo,
            this.columnHeaderMisura,
            this.columnHeaderQta,
            this.columnHeaderPrezzo});
            this.listView1.FullRowSelect = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(0, 28);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(615, 307);
            this.listView1.TabIndex = 1;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.VisibleChanged += new System.EventHandler(this.listView1_VisibleChanged);
            this.listView1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listView1_MouseDoubleClick);
            // 
            // columnHeaderCodice
            // 
            this.columnHeaderCodice.Text = "Cliente";
            this.columnHeaderCodice.Width = 140;
            // 
            // columnHeaderData
            // 
            this.columnHeaderData.Text = "Data";
            this.columnHeaderData.Width = 70;
            // 
            // columnHeaderAzienda
            // 
            this.columnHeaderAzienda.Text = "Azienda";
            this.columnHeaderAzienda.Width = 95;
            // 
            // columnHeaderModello
            // 
            this.columnHeaderModello.Text = "Modello";
            this.columnHeaderModello.Width = 106;
            // 
            // columnHeaderTipo
            // 
            this.columnHeaderTipo.Text = "Tipo";
            this.columnHeaderTipo.Width = 74;
            // 
            // columnHeaderMisura
            // 
            this.columnHeaderMisura.Text = "Misura";
            this.columnHeaderMisura.Width = 80;
            // 
            // columnHeaderQta
            // 
            this.columnHeaderQta.Text = "Qta";
            this.columnHeaderQta.Width = 40;
            // 
            // columnHeaderPrezzo
            // 
            this.columnHeaderPrezzo.Text = "Prezzo";
            this.columnHeaderPrezzo.Width = 80;
            // 
            // VenditeView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "VenditeView";
            this.Size = new System.Drawing.Size(618, 338);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeaderCodice;
        private System.Windows.Forms.ColumnHeader columnHeaderData;
        private System.Windows.Forms.ColumnHeader columnHeaderAzienda;
        private System.Windows.Forms.ColumnHeader columnHeaderModello;
        private System.Windows.Forms.ColumnHeader columnHeaderMisura;
         
        private System.Windows.Forms.ToolStripButton toolStripButtonNuovo;
        private System.Windows.Forms.ToolStripButton toolStripButtonElimina;
        private System.Windows.Forms.ToolStripButton toolStripButtonModifica;
        private System.Windows.Forms.ToolStripButton toolStripButtonConverti;
        private System.Windows.Forms.ColumnHeader columnHeaderQta;
        private System.Windows.Forms.ColumnHeader columnHeaderPrezzo;
        private System.Windows.Forms.ColumnHeader columnHeaderTipo;
        private System.Windows.Forms.ToolStripButton toolStripButtonAdvanced;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButtonStatistiche;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
    }
}
