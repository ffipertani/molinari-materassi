﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using molinari.Models;

namespace molinari.Views
{
    public partial class GenericView : UserControl
    {
        private List<Generic> lastSearch;
        private Form1 parent;
        public GenericView(Form1 parent)
        {
            InitializeComponent();
           //Search()
            this.parent = parent; 
        }

        public GenericView()
        {
            InitializeComponent();
            //Search()

        }
       

        protected virtual List<Generic> DoSearch() { return null; }
        protected virtual Generic DoCreate() { return null; }
        protected virtual void DoInsert(Generic generic){}
        protected virtual void DoUpdate(Generic generic){}
        protected virtual void DoDelete(Generic generic){}
        protected virtual Generic DoLoad(String code) { return null; }
        protected virtual void DoChangeId(Generic generic) { }

        public Image EditImage
        {
            set
            {
                toolStripButtonModifica.Image = value;
            }
        }

        public Image CreateImage
        {
            set
            {
                toolStripButtonNuovo.Image = value;
            }
        }

        public Image DeleteImage
        {
            set
            {
                toolStripButtonElimina.Image = value;
            }
        }

        public void Search()
        {
            lastSearch = DoSearch();
           listView1.Items.Clear();
           foreach (Generic generic in lastSearch)
            {
                listView1.Items.Add(createItem(generic));
            }
           setStatus();
        }


        private void setStatus()
        {
            
            parent.SetStatus("Record totali: " + lastSearch.Count);
        }


        private ListViewItem createItem(Generic generic)
        {
            ListViewItem item = new ListViewItem();
            ListViewItem.ListViewSubItemCollection items = item.SubItems;
            items.Clear();
            items.Add(createItem(item, generic.Codice));
            items.Add(createItem(item, generic.Descrizione));
            return item;
        }

       

        private ListViewItem.ListViewSubItem createItem(ListViewItem item, String text)
        {
            return new ListViewItem.ListViewSubItem(item, text);
        }


        private void toolStripButtonNuovo_Click(object sender, EventArgs e)
        {
            GenericEdit nc = new GenericEdit(DoCreate());
            DialogResult res = nc.ShowDialog(this);
            if (res == DialogResult.OK)
            {
                Generic gen = DoCreate();
                gen.Descrizione = nc.Object.Descrizione;
                DoInsert(gen);
                listView1.Items.Insert(0, createItem(gen));
            }
        }

       

        private void toolStripButtonAggiorna_Click(object sender, EventArgs e)
        {
            Search();
        }

       

        private void toolStripButtonModifica_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                String text = listView1.SelectedItems[0].Text;
                Generic cliente = DoLoad(text);
                GenericEdit ec = new GenericEdit(cliente);
                 DialogResult res = ec.ShowDialog(this);
                if (res == DialogResult.OK)
                {
                    Generic gen = DoCreate();
                    gen.Descrizione = ec.Object.Descrizione;
                    gen.Codice = ec.Object.Codice;
                    DoUpdate(gen);
                    Search();
                }
            }
           

        }

        private void toolStripButtonElimina_Click(object sender, EventArgs e)
        {
            int count =  listView1.SelectedItems.Count;
            if(count>0){
                foreach (ListViewItem item in listView1.SelectedItems)
                {
                    Generic generic = DoCreate();
                    generic.Codice = item.SubItems[0].Text;
                    DoDelete(generic);
                    listView1.Items.Remove(item);
                }
            }

           
        }

        private void toolStripButtonCorreggi_Click(object sender, EventArgs e)
        {
            List<Generic> list = DoSearch();
            foreach (Generic g in list)
            {
                try
                {
                    int cod = Int32.Parse(g.Codice);
                }
                catch (Exception ex)
                {
                    DoChangeId(g);
                }
            }
        }

        private void listView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                String text = listView1.SelectedItems[0].Text;
                Generic cliente = DoLoad(text);
                GenericEdit ec = new GenericEdit(cliente);
                DialogResult res = ec.ShowDialog(this);
                if (res == DialogResult.OK)
                {
                    Generic gen = DoCreate();
                    gen.Descrizione = ec.Object.Descrizione;
                    gen.Codice = ec.Object.Codice;
                    DoUpdate(gen);
                    Search();
                }
            }
        }

        private void GenericView_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible && this.parent!=null)
            {
                setStatus();
            }
        }

      
    }
}
