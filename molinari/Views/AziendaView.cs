﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using molinari.Models;

namespace molinari.Views
{
    public class AziendaView:GenericView
    {
        private System.Windows.Forms.ImageList imageList1;
        private System.ComponentModel.IContainer components;

        public AziendaView(Form1 parent):base(parent)
        {
            InitializeComponent();
            CreateImage = imageList1.Images[0];
            EditImage = imageList1.Images[1];
            DeleteImage = imageList1.Images[2];

        }
        protected override List<Generic> DoSearch()
        {
            return DatabaseManager.SearchAziende().ToList<Generic>();
        }
        protected override Generic DoCreate()
        {
            return new Azienda();
        }
        protected override void DoInsert(Generic generic)
        {
            DatabaseManager.InsertAzienda((Azienda)generic);
        }
        protected override void DoDelete(Generic generic)
        {
            DatabaseManager.DeleteAzienda((Azienda)generic);
        }
        protected override void DoUpdate(Generic generic)
        {
            DatabaseManager.UpdateAzienda((Azienda)generic);
        }
        protected override Generic DoLoad(String code)
        {
            return DatabaseManager.LoadAzienda(code);
        }
        protected override void DoChangeId(Generic generic)
        {
            DatabaseManager.ChangeIdAzienda((Azienda)generic);
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AziendaView));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "lorry-add-icon.png");
            this.imageList1.Images.SetKeyName(1, "lorry-flatbed-icon.png");
            this.imageList1.Images.SetKeyName(2, "lorry-delete-icon.png");
            // 
            // AziendaView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Name = "AziendaView";
            this.ResumeLayout(false);
            this.PerformLayout();

        }
    }
}
