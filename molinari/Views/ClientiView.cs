﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using molinari.Models;

namespace molinari.Views
{
    public partial class ClientiView : UserControl
    {
        private List<Cliente> lastSearch;
        private Form1 parent;

        public ClientiView(Form1 parent)
        {
            InitializeComponent();
            this.parent = parent;
            SearchClienti();
        }

        public ClientiView()
        {
            InitializeComponent();
            SearchClienti();
        }

        public void SearchClienti()
        {
            Console.WriteLine("Searching clienti");
           lastSearch = DatabaseManager.SearchClienti();
           listView1.Items.Clear();
           foreach (Cliente cliente in lastSearch)
            {
                listView1.Items.Add(createItem(cliente));
            }
           setStatus();
           Console.WriteLine("Searching clienti end");
        }

        public void SearchClienti(String text)
        {
            lastSearch = DatabaseManager.SearchClienti(text);
            listView1.Items.Clear();
            foreach (Cliente cliente in lastSearch)
            {
                listView1.Items.Add(createItem(cliente));
            }
            setStatus();
        }


        private void setStatus()
        {
            parent.SetStatus("Record totali: " + lastSearch.Count);
        }

        private ListViewItem createItem(Cliente cliente)
        {
            ListViewItem item = new ListViewItem();
            ListViewItem.ListViewSubItemCollection items = item.SubItems;
            items.Clear();
            items.Add(createItem(item, cliente.Codice));
            items.Add(createItem(item, cliente.Cognome));
            items.Add(createItem(item, cliente.Nome));
            items.Add(createItem(item, cliente.Indirizzo));
            items.Add(createItem(item, cliente.Citta));
            items.Add(createItem(item, cliente.Recapito));
            return item;
        }

       

        private ListViewItem.ListViewSubItem createItem(ListViewItem item, String text)
        {
            return new ListViewItem.ListViewSubItem(item, text);
        }

        private void toolStripButtonNuovo_Click(object sender, EventArgs e)
        {
            NuovoCliente nc = new NuovoCliente();
            DialogResult res = nc.ShowDialog(this);
            if (res == DialogResult.OK)
            {
                DatabaseManager.InsertCliente(nc.Cliente);
                listView1.Items.Insert(0, createItem(nc.Cliente));
            }
        }

        private void toolStripButtonAggiorna_Click(object sender, EventArgs e)
        {
            String text = toolStripTextBoxSearch.Text;
            SearchClienti(text);
        }

        private void toolStripButtonModifica_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                String text = listView1.SelectedItems[0].Text;
                Cliente cliente = DatabaseManager.LoadCliente(text);
                EditCliente ec = new EditCliente(cliente);
                 DialogResult res = ec.ShowDialog(this);
                if (res == DialogResult.OK)
                {
                    DatabaseManager.UpdateCliente(ec.Cliente);
                    SearchClienti();
                }
            }
           

        }

        private void toolStripButtonElimina_Click(object sender, EventArgs e)
        {
            int count =  listView1.SelectedItems.Count;
            if(count>0){
                foreach (ListViewItem item in listView1.SelectedItems)
                {
                    Cliente cliente = new Cliente();
                    cliente.Codice = item.SubItems[0].Text;
                    DatabaseManager.DeleteCliente(cliente);
                    listView1.Items.Remove(item);
                }
            }

           
        }

        private void toolStripButtonSistemaCaratteri_Click(object sender, EventArgs e)
        {
            List<Cliente> clienti = DatabaseManager.SearchClienti();
            foreach (Cliente cliente in clienti)
            {
                if (cliente.Indirizzo.Contains((char)65533))
                {
                    cliente.Indirizzo = cliente.Indirizzo.Replace((char)65533, '°');
                    DatabaseManager.UpdateCliente(cliente);
                }
                
            }
        }

        private void toolStripButtonId_Click(object sender, EventArgs e)
        {
            DatabaseManager.SistemaIdClienti();
            SearchClienti();
        }

        private void toolStripTextBoxSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                String text = toolStripTextBoxSearch.Text;
                SearchClienti(text);
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
          
        }

        private void listView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                String text = listView1.SelectedItems[0].Text;
                Cliente cliente = DatabaseManager.LoadCliente(text);
                EditCliente ec = new EditCliente(cliente);
                DialogResult res = ec.ShowDialog(this);
                if (res == DialogResult.OK)
                {
                    DatabaseManager.UpdateCliente(ec.Cliente);
                    SearchClienti();
                }
            }
        }

        private void ClientiView_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                setStatus();
            }
        }

       
    }
}
