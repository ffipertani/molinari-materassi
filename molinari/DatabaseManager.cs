﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using molinari.Dao;
using molinari.Models;

namespace molinari
{
    class DatabaseManager
    {

        static ClienteDao clienteDao = new ClienteDao();
        static AziendaDao aziendaDao = new AziendaDao();
        static MisuraDao misuraDao = new MisuraDao();
        static ModelloDao modelloDao = new ModelloDao();
        static TipoDao tipoDao = new TipoDao();
        static VenditaDao venditaDao = new VenditaDao();

        //Clienti/////////////
        public static List<Cliente> SearchClienti()
        {
            return clienteDao.SearchClienti();
        }

        public static List<Cliente> SearchClienti(String text)
        {
            return clienteDao.SearchClienti(text);
        }

        public static Cliente LoadCliente(String codice)
        {
            return clienteDao.LoadCliente(codice);
        }

        public static void InsertCliente(Cliente cliente)
        {
            clienteDao.InsertCliente(cliente);
        }

        public static void DeleteCliente(Cliente cliente)
        {
            clienteDao.DeleteCliente(cliente);
        }

        public static void UpdateCliente(Cliente cliente)
        {
            clienteDao.UpdateCliente(cliente);
        }
        public static void SistemaIdClienti()
        {
            clienteDao.SistemaIdClienti();
        }

        //Vendite/////////////
        public static List<Vendita> SearchVendite()
        {
            return venditaDao.SearchVendite();
        }

        public static List<Vendita> SearchVendite(VenditaSearch vs)
        {
            return venditaDao.SearchVendite(vs);
        }

        public static Vendita LoadVendita(String codice)
        {
            return venditaDao.LoadVendita(codice);
        }

        public static void InsertVendita(Vendita cliente)
        {
            venditaDao.InsertVendita(cliente);
        }

        public static void DeleteVendita(Vendita cliente)
        {
            venditaDao.DeleteVendita(cliente);
        }

        public static void UpdateVendita(Vendita cliente)
        {
            venditaDao.UpdateVendita(cliente);
        }

        public static void ConvertDataVendite()
        {
            venditaDao.ConvertDataVendita();
        }

        //Aziende/////////////
        public static List<Azienda> SearchAziende()
        {
            return aziendaDao.Search();
        }

        public static Azienda LoadAzienda(String codice)
        {
            return aziendaDao.Load(codice);
        }

        public static void InsertAzienda(Azienda azienda)
        {
            aziendaDao.Insert(azienda);
        }

        public static void DeleteAzienda(Azienda azienda)
        {
            aziendaDao.Delete(azienda);
        }

        public static void UpdateAzienda(Azienda azienda)
        {
            aziendaDao.Update(azienda);
        }

        public static void ChangeIdAzienda(Azienda azienda)
        {
            String code = aziendaDao.Next();
            aziendaDao.UpdateVendite(azienda.Codice, code);
        }

        //Misura/////////////
        public static List<Misura> SearchMisure()
        {
            return misuraDao.Search();
        }

        public static Misura LoadMisura(String codice)
        {
            return misuraDao.Load(codice);
        }

        public static void InsertMisura(Misura misura)
        {
            misuraDao.Insert(misura);
        }

        public static void DeleteMisura(Misura misura)
        {
            misuraDao.Delete(misura);
        }

        public static void UpdateMisura(Misura misura)
        {
            misuraDao.Update(misura);
        }

        public static void ChangeIdMisura(Misura misura)
        {
            String code = misuraDao.Next();
            misuraDao.UpdateVendite(misura.Codice, code);
        }

        //Modello/////////////
        public static List<Modello> SearchModelli()
        {
            return modelloDao.Search();
        }

        public static Modello LoadModello(String codice)
        {
            return modelloDao.Load(codice);
        }

        public static void InsertModello(Modello modello)
        {
            modelloDao.Insert(modello);
        }

        public static void DeleteModello(Modello modello)
        {
            modelloDao.Delete(modello);
        }

        public static void UpdateModello(Modello modello)
        {
            modelloDao.Update(modello);
        }

        public static void ChangeIdModello(Modello modello)
        {
            String code = modelloDao.Next();
            modelloDao.UpdateVendite(modello.Codice,code);
        }

        //Tipi/////////////
        public static List<Tipo> SearchTipi()
        {
            return tipoDao.Search();
        }

        public static Tipo LoadTipo(String codice)
        {
            return tipoDao.Load(codice);
        }

        public static void InsertTipo(Tipo tipo)
        {
            tipoDao.Insert(tipo);
        }

        public static void DeleteTipo(Tipo tipo)
        {
            tipoDao.Delete(tipo);
        }

        public static void UpdateTipo(Tipo tipo)
        {
            tipoDao.Update(tipo);
        }

        public static void ChangeIdTipo(Tipo tipo)
        {
            String code = tipoDao.Next();
            tipoDao.UpdateVendite(tipo.Codice, code);
        }
    }
}
