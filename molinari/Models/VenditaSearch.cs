﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace molinari.Models
{
    public class VenditaSearch
    {
        public DateTime Da { get; set; }
        public DateTime A { get; set; }
        public List<Azienda> Aziende { get; set; }
        public List<Misura> Misure { get; set; }
        public List<Modello> Modelli { get; set; }
        public List<Tipo> Tipi { get; set; }
        public List<Cliente> Clienti { get; set; }
    }
}
