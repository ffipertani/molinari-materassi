﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace molinari.Models
{
    public class Vendita
    {
        public Vendita()
        {
            Tipo = new Tipo();
            Modello = new Modello();
            Misura = new Misura();
            Azienda = new Azienda();
            Cliente = new Cliente();
        }
        public String IdMovim { get; set; }
     //   public String Codice { get; set; }
        public String Data { get; set; }
        public Tipo Tipo { get; set; }
        public Modello Modello { get; set; }
        public Misura Misura { get; set; }
        public Azienda Azienda { get; set; }
        public String Prezzo { get; set; }
        public String Qta { get; set; }
        public Cliente Cliente { get; set; }
      
    }
}
