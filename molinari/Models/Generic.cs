﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace molinari.Models
{
    public class Generic
    {
        public String Codice { get; set; }
        public String Descrizione { get; set; }

        public override String ToString()
        {
            return Descrizione;
        }
    }
}
