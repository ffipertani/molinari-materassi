﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace molinari.Models
{
    public class Cliente
    {
        public String Codice { get; set; }
        public String Nome { get; set; }
        public String Cognome { get; set; }
        public String Indirizzo { get; set; }
        public String Citta { get; set; }
        public String Recapito { get; set; }
    }
}
