﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using molinari.Views;

namespace molinari
{
    public partial class Form1 : Form
    {
        Dictionary<String, Control> views = new Dictionary<string, Control>();
        public Form1()
        {
            InitializeComponent();
            treeView1.BeforeSelect += treeView1_BeforeSelect;
            views.Add("Vendite", new VenditeView(this));
            views.Add("Clienti", new ClientiView(this));            
            views.Add("Aziende", new AziendaView(this));
            views.Add("Misure", new MisureView(this));
            views.Add("Modelli", new ModelliView(this));
            views.Add("Tipi", new TipiView(this));
        }

        public void SetStatus(String text)
        {
            this.statusLabel.Text = text;
        }

        void treeView1_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            Control value;
            views.TryGetValue(e.Node.Text, out value);
            if (value == null)
            {
                MessageBox.Show(e.Node.Text);
            }
            else
            {
                splitContainer1.Panel2.Controls.Clear();
                splitContainer1.Panel2.Controls.Add(value);
                value.Location = new Point(0, 0);
                value.Size = new System.Drawing.Size(splitContainer1.Panel2.Width, splitContainer1.Panel2.Height);
                value.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
                splitContainer1.Panel2.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;


                if (value is GenericView)
                {
                    ((GenericView)value).Search();
                }
            }
        }

        private void esciToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
