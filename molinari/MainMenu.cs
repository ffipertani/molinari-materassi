﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace molinari
{
    class MainMenu:TreeView
    {
        public MainMenu()
        {
           
            Nodes.Clear();
            Add("Clienti",0);
            Add("Vendite",1);
          //  Add("Statistiche",2);
            TreeNode treeNode = Add("Configurazione          ",3);
            treeNode.NodeFont = new Font(Font.FontFamily, 8, FontStyle.Bold);
            Add(treeNode.Nodes,"Tipi",4);
            Add(treeNode.Nodes, "Modelli",5);
            Add(treeNode.Nodes, "Misure",6);
            Add(treeNode.Nodes, "Aziende",7);
             

            // this.Indent = 30;
           // this.ItemHeight = 20;
            //this.ItemHeight = 35;
            this.ShowLines = false;
            this.ShowPlusMinus = false;
            this.ExpandAll();
            this.HideSelection = false;
            
        }
        protected override void OnAfterSelect(TreeViewEventArgs e)
        {
            this.SelectedImageIndex = e.Node.ImageIndex;
            base.OnAfterSelect(e);
            this.ExpandAll();
        }
        

        private TreeNode Add(String text,int index)
        {
            return Add(Nodes, text,index);
        }

        private TreeNode Add(TreeNodeCollection nodes,String text,int index)
        {
            TreeNode child = new TreeNode(text);
            child.ImageIndex = index;
            
            child.NodeFont = new Font(Font.FontFamily, 8, FontStyle.Regular);
            nodes.Add(child);
            return child;
        }
       

        int WM_LBUTTONDOWN = 0x0201; //513
        int WM_LBUTTONUP = 0x0202; //514
        int WM_LBUTTONDBLCLK = 0x0203; //515

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_LBUTTONDOWN ||
               m.Msg == WM_LBUTTONUP ||
               m.Msg == WM_LBUTTONDBLCLK)
            {
                //Get cursor position(in client coordinates)
                Int16 x = (Int16)m.LParam;
                Int16 y = (Int16)((int)m.LParam >> 16);

                // get infos about the location that will be clicked
                var info = this.HitTest(x, y);

                // if the location is a node
                if (info.Node != null)
                {
                    if (info.Node.Text == "Configurazione          ")
                        return;
                }
            }

            //Dispatch as usual
            base.WndProc(ref m);
        }
    }
}
