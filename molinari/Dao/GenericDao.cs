﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using molinari.Models;

namespace molinari.Dao
{
    public class GenericDao<T>:BaseDao where T: Generic
    {
        private String tableName;
        private Type modelType;
        private String columnName;

        public GenericDao(String tableName, Type modelType, String columnName)
        {
            this.tableName = tableName;
            this.modelType = modelType;
            this.columnName = columnName;
        }

        public String Next()
        {
            int max = 0;
            List<T> list = Search();
            foreach (T c in list)
            {
                try
                {
                    int val = Int32.Parse(c.Codice);
                    if (val > max)
                    {
                        max = val;

                    }
                }
                catch (Exception e) 
                {
                    Console.WriteLine("Attenzione id non numerico");
                }
            }
            max++;
            return max.ToString();
        }


        public List<T> Search()
        {
            List<T> list = new List<T>();
            DataTable dt = GetDataTable("SELECT * FROM " + tableName + " order by Descrizione ASC");
            foreach (DataRow dr in dt.Rows)
            {
                T c = (T)Activator.CreateInstance(modelType);
                c.Codice = getString(dr.ItemArray[0]);
                c.Descrizione = getString(dr.ItemArray[1]);

                list.Add(c);
            }
            return list;
        }

        public T Load(String codice)
        {
            DataTable dt = GetDataTable("SELECT * FROM "+tableName+" where codice = '" + codice + "'");
            T c = (T)Activator.CreateInstance(modelType);
            foreach (DataRow dr in dt.Rows)
            {
                c.Codice = getString(dr.ItemArray[0]);
                c.Descrizione = getString(dr.ItemArray[1]);
            }
            return c;
        }

        public void Insert(T generic)
        {
            generic.Codice = Next();
            string sql = "insert into " + tableName + " (Codice,Descrizione) values ('" + generic.Codice + "', '" + generic.Descrizione + "')";
            SQLiteCommand command = new SQLiteCommand(sql, Connect());
            int res = command.ExecuteNonQuery();
            if (res == 0) { }
        }

        public void Delete(T generic)
        {
            string sql = "delete from " + tableName + " where Codice = '" + generic.Codice + "'";
            SQLiteCommand command = new SQLiteCommand(sql, Connect());
            int res = command.ExecuteNonQuery();
            if (res == 0) { }
        }

        public void Update(T generic)
        {
            SQLiteConnection cnn = Connect();
            string sql = String.Format("update " + tableName + " set Descrizione='{0}' where codice = '{1}'", generic.Descrizione, generic.Codice);
            SQLiteCommand command = new SQLiteCommand(sql, cnn);
            int res = command.ExecuteNonQuery();
            if (res == 0) { }
            cnn.Close();
        }

        public void UpdateVendite(String oldCode, String newCode)
        {
            SQLiteConnection cnn = Connect();
            string sql = String.Format("update MOVIM  set "+columnName+"='{0}' where "+columnName+" = '{1}'", newCode, oldCode);
            SQLiteCommand command = new SQLiteCommand(sql, cnn);
            int res = command.ExecuteNonQuery();
            if (res == 0) { }
            sql = String.Format("update " + tableName + "  set Codice ='{0}' where Codice = '{1}'", newCode, oldCode);
            command = new SQLiteCommand(sql, cnn);
            res = command.ExecuteNonQuery();
            if (res == 0) { }
            cnn.Close();
        }
    }
}
