﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using molinari.Models;

namespace molinari.Dao
{
    public class VenditaDao:BaseDao
    {
        TipoDao tipoDao = new TipoDao();
        ModelloDao modelloDao = new ModelloDao();
        MisuraDao misuraDao = new MisuraDao();
        AziendaDao aziendaDao = new AziendaDao();
        ClienteDao clienteDao = new ClienteDao();

        private String NextVendita()
        {
            int max = 0;
            List<Vendita> list = SearchVendite();
            foreach (Vendita c in list)
            {
                int val = Int32.Parse(c.IdMovim);
                if (val > max)
                {
                    max = val;

                }
            }
            max++;
            return max.ToString();
        }

        public void CreateId()
        {
            List<Vendita> vendite = SearchVendite();
            SQLiteConnection cnn = Connect();
            int rowid=2;
            for(int i=0;i<vendite.Count;i++){
                string sql = String.Format("update MOVIM set IdMovim='{0}' where rowid = '{1}'", i, rowid++);
                 SQLiteCommand command = new SQLiteCommand(sql, cnn);
                    int res = command.ExecuteNonQuery();
                    if (res == 0) { }
            }
             
        }

        public List<Vendita> SearchVendite()
        {
            List<Modello> cacheModelli = new List<Modello>();
            List<Tipo> cacheTipi = new List<Tipo>();
            List<Misura> cacheMisure = new List<Misura>();
            List<Azienda> cacheAziende= new List<Azienda>();
            List<Cliente> cacheCliente = new List<Cliente>();

            List<Vendita> list = new List<Vendita>();
            DataTable dt = GetDataTable("SELECT * FROM MOVIM");
            foreach (DataRow dr in dt.Rows)
            {

                Vendita c = new Vendita();
                c.Cliente = loadCliente(cacheCliente,getString(dr.ItemArray[0]));
                c.Data = getString(dr.ItemArray[1]);
                c.Tipo = loadTipo(cacheTipi,getString(dr.ItemArray[2]));
                c.Modello = loadModello(cacheModelli,(getString(dr.ItemArray[3])));
                c.Misura = loadMisura(cacheMisure,getString(dr.ItemArray[4]));
                c.Prezzo = getString(dr.ItemArray[5]);
                c.Qta = getString(dr.ItemArray[6]);
                c.Azienda =loadAzienda(cacheAziende,getString(dr.ItemArray[7]));
                c.IdMovim = getString(dr.ItemArray[8]);
                list.Add(c);
            }
            return list;
        }

        public List<Vendita> SearchVendite(VenditaSearch vs)
        {
            List<Modello> cacheModelli = new List<Modello>();
            List<Tipo> cacheTipi = new List<Tipo>();
            List<Misura> cacheMisure = new List<Misura>();
            List<Azienda> cacheAziende = new List<Azienda>();
            List<Cliente> cacheCliente = new List<Cliente>();
           
            if (vs.A.Day == 1 && vs.A.Month==1 && vs.A.Year == 1)
            {
                vs.A = new DateTime(2222, 2, 2);
            }
            List<Vendita> list = new List<Vendita>();
            String query = "SELECT * FROM MOVIM where ( DATE(substr(Data,7,4)||substr(Data,4,2)||substr(Data,1,2)) BETWEEN DATE(" + vs.Da.ToString("yyyyMMdd") + ") AND DATE(" + vs.A.ToString("yyyyMMdd") + "))";

            if (vs.Aziende != null)
            {
                query += " AND Azienda in (";
                foreach (Azienda azienda in vs.Aziende)
                {
                    query += "'"+azienda.Codice + "',";
                }
                if (query[query.Length-1].Equals(','))
                {
                    query = query.Remove(query.Length - 1);
                }
                query += ")";
            }
            if (vs.Misure != null)
            {
                query += " AND Misura in (";
                foreach (Misura misura in vs.Misure)
                {
                    query += "'" + misura.Codice + "',";
                }
                if (query[query.Length - 1].Equals(','))
                {
                    query = query.Remove(query.Length - 1);
                }
                query += ")";
            }
            if (vs.Tipi != null)
            {
                query += " AND Tipo in (";
                foreach (Tipo tipo in vs.Tipi)
                {
                    query += "'" + tipo.Codice + "',";
                }
                if (query[query.Length - 1].Equals(','))
                {
                    query = query.Remove(query.Length - 1);
                }
                query += ")";
            }
            if (vs.Modelli != null)
            {
                query += " AND Modello in (";
                foreach (Modello modello in vs.Modelli)
                {
                    query += "'" + modello.Codice + "',";
                }
                if (query[query.Length - 1].Equals(','))
                {
                    query = query.Remove(query.Length - 1);
                }
                query += ")";
            }
            if (vs.Clienti != null)
            {
                query += " AND Codice in (";
                foreach (Cliente cliente in vs.Clienti)
                {
                    query += "'" + cliente.Codice + "',";
                }
                if (query[query.Length - 1].Equals(','))
                {
                    query = query.Remove(query.Length - 1);
                }
                query += ")";
            }
            
            DataTable dt = GetDataTable(query);
            foreach (DataRow dr in dt.Rows)
            {

                Vendita c = new Vendita();
                c.Cliente = loadCliente(cacheCliente, getString(dr.ItemArray[0]));
                c.Data = getString(dr.ItemArray[1]);
                c.Tipo = loadTipo(cacheTipi, getString(dr.ItemArray[2]));
                c.Modello = loadModello(cacheModelli, (getString(dr.ItemArray[3])));
                c.Misura = loadMisura(cacheMisure, getString(dr.ItemArray[4]));
                c.Prezzo = getString(dr.ItemArray[5]);
                c.Qta = getString(dr.ItemArray[6]);
                c.Azienda = loadAzienda(cacheAziende, getString(dr.ItemArray[7]));
                c.IdMovim = getString(dr.ItemArray[8]);
                list.Add(c);
            }
            return list;
        }

        public void ConvertDataVendita()
        {
            List<Vendita> vendite = SearchVendite();
            SQLiteConnection cnn = Connect();
            for(int i=0;i<vendite.Count;i++)
            {
                try
                {
                    Vendita vendita = vendite[i];
                    DateTime old = DateTime.Parse(vendita.Data);
                    vendita.Data = old.ToString("dd/MM/yyyy");
                   
                    string sql = String.Format("update MOVIM set Data='{0}' where IdMovim = '{1}'", vendita.Data,vendita.IdMovim);
                    SQLiteCommand command = new SQLiteCommand(sql, cnn);
                    int res = command.ExecuteNonQuery();
                    if (res == 0) { }
                   
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
            cnn.Close();
        }

        private Cliente loadCliente(List<Cliente> cache, String codice)
        {
            foreach (Cliente cliente in cache)
            {
                if (cliente.Codice!=null && cliente.Codice.Equals(codice))
                {
                   return cliente;
                }
            }
            Cliente c = clienteDao.LoadCliente(codice);
            cache.Add(c);
            return c;
        }

        private Modello loadModello(List<Modello> cache, String codice)
        {
            foreach (Modello m in cache)
            {
                if (m.Codice!=null && m.Codice.Equals(codice))
                {
                    return m;
                }
            }
            Modello mo = modelloDao.Load(codice);
            cache.Add(mo);
            return mo;
        }

        private Tipo loadTipo(List<Tipo> cache, String codice)
        {
            foreach (Tipo m in cache)
            {
                if (m.Codice != null && m.Codice.Equals(codice))
                {
                    return m;
                }
            }
            Tipo mo = tipoDao.Load(codice);
            cache.Add(mo);
            return mo;
        }

        private Misura loadMisura(List<Misura> cache, String codice)
        {
            foreach (Misura m in cache)
            {
                if (m.Codice != null && m.Codice.Equals(codice))
                {
                    return m;
                }
            }
            Misura mo = misuraDao.Load(codice);
            cache.Add(mo);
            return mo;
        }

        private Azienda loadAzienda(List<Azienda> cache, String codice)
        {
            foreach (Azienda m in cache)
            {
                if (m.Codice != null && m.Codice.Equals(codice))
                {
                    return m;
                }
            }
            Azienda mo = aziendaDao.Load(codice);
            cache.Add(mo);
            return mo;
        }


        public Vendita LoadVendita(String idMovim)
        {
         
            DataTable dt = GetDataTable("SELECT * FROM MOVIM where idMovim = '"+idMovim+"' " );
            Vendita c = new Vendita();
            foreach (DataRow dr in dt.Rows)
            {
                c.Cliente = clienteDao.LoadCliente(getString(dr.ItemArray[0]));
                c.Data = getString(dr.ItemArray[1]);
                c.Tipo = tipoDao.Load(getString(dr.ItemArray[2]));
                c.Modello = modelloDao.Load(getString(dr.ItemArray[3]));
                c.Misura = misuraDao.Load(getString(dr.ItemArray[4]));
                c.Prezzo = getString(dr.ItemArray[5]);
                c.Qta = getString(dr.ItemArray[6]);
                c.Azienda = aziendaDao.Load(getString(dr.ItemArray[7]));
                c.IdMovim = getString(dr.ItemArray[8]);
            }
            return c;
        }

        public void InsertVendita(Vendita vendita)
        {
            SQLiteConnection cnn = Connect();
            vendita.IdMovim = NextVendita();
            string sql = "insert into MOVIM (IdMovim, Codice,Data, Tipo, Modello, Misura, Prezzo, Qta, Azienda) values ('" + vendita.IdMovim + "','" + vendita.Cliente.Codice + "', '" + vendita.Data + "','" + vendita.Tipo.Codice + "','" + vendita.Modello.Codice + "','" + vendita.Misura.Codice + "','" + vendita.Prezzo + "','" + vendita.Qta + "','" + vendita.Azienda.Codice + "')";
            SQLiteCommand command = new SQLiteCommand(sql, Connect());
            int res = command.ExecuteNonQuery();
            if (res == 0) { }
            cnn.Close();
        }

        public void DeleteVendita(Vendita vendita)
        {
            SQLiteConnection cnn = Connect();
            string sql = "delete from MOVIM where IdMovim = '" + vendita.IdMovim + "'";
            SQLiteCommand command = new SQLiteCommand(sql, cnn);
            int res = command.ExecuteNonQuery();
            if (res == 0) { }
            cnn.Close();
        }

        public void UpdateVendita(Vendita vendita)
        {
            SQLiteConnection cnn = Connect();
            string sql = String.Format("update MOVIM set Data='{0}', Tipo='{1}', Modello='{2}', Misura='{3}', Prezzo='{4}', Qta='{5}', Azienda='{6}', Codice='{7}' where IdMovim = '{8}'", vendita.Data, vendita.Tipo.Codice, vendita.Modello.Codice, vendita.Misura.Codice, vendita.Prezzo, vendita.Qta, vendita.Azienda.Codice,vendita.Cliente.Codice,vendita.IdMovim);
            SQLiteCommand command = new SQLiteCommand(sql,cnn);
            int res = command.ExecuteNonQuery();
            if (res == 0) { }
            cnn.Close();
        }
    }
}
