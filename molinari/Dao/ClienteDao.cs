﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using molinari.Models;

namespace molinari.Dao
{
    public class ClienteDao:BaseDao
    {
        private String NextCliente()
        {
            int max = 0;
            List<Cliente> list = SearchClienti();
            foreach (Cliente c in list)
            {
                int val = Int32.Parse(c.Codice);
                if (val > max)
                {
                    max = val;

                }
            }
            max++;
            return max.ToString();
        }

        public List<Cliente> SearchClienti(String text)
        {
            List<Cliente> list = new List<Cliente>();
            String[] split = text.Split(new string[] { " " }, StringSplitOptions.None);
            String query = "SELECT * FROM ANAGCLI where ";
            foreach (String par in split)
            {
                query += "(Cognome like '%" + par + "%' or Nome like '%" + par + "%' or Nome like '%" + par + "%' or Indirizzo like '%" + par + "%' or Citta like '%" + par + "%' or Recapito like '%" + par + "%') AND";
            }
            if (query.EndsWith("AND"))
            {
                query = query.Remove(query.Length - 4);
            }

            DataTable dt = GetDataTable(query);
            foreach (DataRow dr in dt.Rows)
            {
                Cliente c = new Cliente();
                c.Codice = getString(dr.ItemArray[0]);
                c.Cognome = getString(dr.ItemArray[1]);
                c.Nome = getString(dr.ItemArray[2]);
                c.Indirizzo = getString(dr.ItemArray[3]);
                c.Citta = getString(dr.ItemArray[4]);
                c.Recapito = getString(dr.ItemArray[5]);

                list.Add(c);
            }
            return list;
        }

        public List<Cliente> SearchClienti()
        {
            List<Cliente> list = new List<Cliente>();
            DataTable dt = GetDataTable("SELECT * FROM ANAGCLI");
            foreach (DataRow dr in dt.Rows)
            {

                Cliente c = new Cliente();
                c.Codice = getString(dr.ItemArray[0]);
                c.Cognome = getString(dr.ItemArray[1]);
                c.Nome = getString(dr.ItemArray[2]);
                c.Indirizzo = getString(dr.ItemArray[3]);
                c.Citta = getString(dr.ItemArray[4]);
                c.Recapito = getString(dr.ItemArray[5]);

                list.Add(c);
            }
            return list;
        }

        public Cliente LoadCliente(String codice)
        {
             
            DataTable dt = GetDataTable("SELECT * FROM ANAGCLI where codice = '"+codice+"'");
                Cliente c = new Cliente();
            foreach (DataRow dr in dt.Rows)
            {

                
                c.Codice = getString(dr.ItemArray[0]);
                c.Cognome = getString(dr.ItemArray[1]);
                c.Nome = getString(dr.ItemArray[2]);
                c.Indirizzo = getString(dr.ItemArray[3]);
                c.Citta = getString(dr.ItemArray[4]);
                c.Recapito = getString(dr.ItemArray[5]);

                
            }
            return c;
        }

        public void SistemaIdClienti()
        {
            List<String> used = new List<String>();
            List<Cliente> clienti = SearchClienti();
 
            foreach (Cliente cliente in clienti)
            {
                Boolean found = false;
                foreach (String id in used)
                {
                    if (id.Equals(cliente.Codice))
                    {
                        found = true;
                        break;
                    }
                }
                if (found)
                {
                    String newId = NextCliente();
                    SQLiteConnection cnn = Connect();
                    string sql = String.Format("update ANAGCLI set Codice='{0}' where Codice = '{1}' and Cognome = '{2}'", newId, cliente.Codice, str(cliente.Cognome));
                    SQLiteCommand command = new SQLiteCommand(sql, cnn);
                    int res = command.ExecuteNonQuery();
                    if (res == 0) { }
                    cnn.Close();
                    used.Add(newId);
                }
                else
                {
                    used.Add(cliente.Codice);
                }
            }
        }

        public void InsertCliente(Cliente cliente)
        {
            SQLiteConnection cnn = Connect();
            cliente.Codice = NextCliente();
            string sql = "insert into ANAGCLI (Codice,Cognome, Nome, Indirizzo, Citta, Recapito) values ('" + cliente.Codice + "', '" + str(cliente.Cognome) + "','" + str(cliente.Nome) + "','" + str(cliente.Indirizzo) + "','" + str(cliente.Citta) + "','" + str(cliente.Recapito) + "')";
            SQLiteCommand command = new SQLiteCommand(sql, Connect());
            int res = command.ExecuteNonQuery();
            if (res == 0) { }
            cnn.Close();
        }

        public void DeleteCliente(Cliente cliente)
        {
            SQLiteConnection cnn = Connect();
            string sql = "delete from ANAGCLI where Codice = '" + cliente.Codice + "'";
            SQLiteCommand command = new SQLiteCommand(sql, cnn);
            int res = command.ExecuteNonQuery();
            if (res == 0) { }
            cnn.Close();
        }

        public void UpdateCliente(Cliente cliente)
        {
            SQLiteConnection cnn = Connect();
            string sql = String.Format("update ANAGCLI set Cognome='{0}', Nome='{1}', Indirizzo='{2}', Citta='{3}', Recapito='{4}' where Codice = '{5}'", str(cliente.Cognome), str(cliente.Nome), str(cliente.Indirizzo), str(cliente.Citta), str(cliente.Recapito), cliente.Codice);
            SQLiteCommand command = new SQLiteCommand(sql,cnn);
            int res = command.ExecuteNonQuery();
            if (res == 0) { }
            cnn.Close();
        }

       
    }
}
