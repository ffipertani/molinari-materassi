﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using molinari.Models;

namespace molinari.Dao
{
    public class TipoDao:GenericDao<Tipo>
    {
        public TipoDao() : base("TIPI", typeof(Tipo), "Tipo") { }
    }
}
