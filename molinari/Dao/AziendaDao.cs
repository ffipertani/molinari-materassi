﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using molinari.Models;

namespace molinari.Dao
{
    public class AziendaDao:GenericDao<Azienda>
    {
        public AziendaDao():base("AZIENDE",typeof(Azienda),"Azienda"){}
    }
}
