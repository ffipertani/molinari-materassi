﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using molinari.Models;

namespace molinari.Dao
{
    public class MisuraDao:GenericDao<Misura>
    {
        public MisuraDao() : base("MISURE", typeof(Misura),"Misura") { }
    }
}
