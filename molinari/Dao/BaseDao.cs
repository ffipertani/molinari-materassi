﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace molinari.Dao
{
    public class BaseDao
    {
        private static String filename = @".\molinari.sqlite";
        private static SQLiteConnection connection = null;
       

        protected DataTable GetDataTable(string sql)
        {
            DataTable dt = new DataTable();
            try
            {
                SQLiteConnection cnn = Connect();
                
                SQLiteCommand mycommand = new SQLiteCommand(cnn);
                mycommand.CommandText = sql;
                SQLiteDataReader reader = mycommand.ExecuteReader();
                dt.Load(reader);
                reader.Close();
                //cnn.Close();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return dt;
        }

        protected SQLiteConnection Connect()
        {
            if (connection == null || connection.State == ConnectionState.Closed) { 
                FileInfo fi = new FileInfo(filename);

                Console.WriteLine("Starting connection " + fi.FullName);
                SQLiteConnection dbConnection = new SQLiteConnection("Data Source=" + filename + ";Version=3;");

                dbConnection.Open();
                Console.WriteLine("connection ok");
                connection = dbConnection;
                return dbConnection;
            }
            return connection;
        }

        protected String str(String str)
        {
            if (str == null)
            {
                return null;
            }
            return str.Replace("'", "''");
        }

        protected String getString(object o)
        {
            if (o is String)
            {
                return (String)o;
            }
            return "";
        }
    }
}
