﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using molinari.Models;

namespace molinari.Dao
{
    public class ModelloDao:GenericDao<Modello>
    {
        public ModelloDao() : base("MODELLI", typeof(Modello),"Modello") { }
    }
}
